Pesquisa sobre estrutura da galeria
===================================

Pesquisa para criação do projeto da galeria do ISA.

Sobre a galeria expandida:
--------------------------

* [Svelte](https://svelte.dev/examples#onmount), design simples
* [Vue.js](https://vuejsexamples.com/pure-js-lightbox-component-for-vue-js/), design completo com setas, contagem e edição
* [Vue.js](https://vuejsexamples.com/vuejs-responsive-and-customizable-image-and-video-gallery-2/), inclui diferentes tamanhos de imagem na visualização, com setas
* [Vue.js](https://vuejsexamples.com/responsive-gallery-component-for-vuejs/), visualização com prévias
* [Vue.js](https://vuejsexamples.com/a-vue-plugin-that-displays-a-gallery-of-image-with-swipe-function/), diferentes tamanhos de imagem na visualizaçãoo, função swipe com legenda, setas, contagem e zoom


Sobre a galeria compacta:
-------------------------

* [Svelte](https://svelte.dev/examples#media-elements), incluindo slideshow
* [Vue.js](https://asvrada.github.io/vue-imagewall/), mais imagens sendo exibidas ao mesmo tempo
* [Vue.js](https://vuejsexamples.com/a-vue2-plugin-for-images-show-in-gallery-or-carousel/), exibe mais imagens em menos espaço


Recursos extras:
----------------

* [Svelte](https://svelte.dev/examples#dimensions), possível redimensionamento das imagens do slideshow
* [Svelte](https://svelte.dev/examples#tweened), possível contagem da galeria
* [Svelte](https://svelte.dev/examples#slots), possível caixa de imagem + legenda/texto explicativo


Outras implementações
---------------------

* jQuery:
  * [Colorbox](http://www.jacklmoore.com/colorbox/).
  * [Diretório de plugins para jQuery](https://www.jqueryscript.net/gallery/).
  * [Vários plugins para jQuery](https://speckyboy.com/free-jquery-image-gallery-lightbox-plugins/).
  * [Top 50 de 2019](https://1stwebdesigner.com/jquery-gallery/).
  * [Top 30](https://jqueryhouse.com/best-jquery-image-gallery-plugins/).
* Bootstrap:
  * [Lightbox for Bootstrap](https://ashleydw.github.io/lightbox/).
* Javascript:
  * [Spotlight](https://nextapps-de.github.io/spotlight/).
  * [Jyicebox](https://www.juicebox.net/).
  * [Showkase](https://www.showkase.net/).
* Vue.js:
  * [Diversas galerias](https://www.vuescript.com/gallery/).
  * [vlightbox](https://vuejsfeed.com/blog/create-an-image-gallery-with-the-lightbox-component-for-vue-js).


Outros lugares para buscar
--------------------------

* [Busca sobre galerias no NPM](https://www.npmjs.com/search?q=image%20gallery).
* [Microjs](http://microjs.com/).
