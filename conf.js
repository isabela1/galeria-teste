var xmlhttp = new XMLHttpRequest();
var json = "config.json";
var config;

function action(config){
	var tag = getQueryVariable('tag');

	for (x in config.all){
		for (var i = 0; i < config.all[x].tags.length; i++){
			if (tag == config.all[x].tags[i]){
				var father = document.body;
				var ctdImg = document.createElement('img');
				ctdImg.src = "/images/" + config.all[x].img;
				father.appendChild(ctdImg);
			}
		}
	}
}

xmlhttp.onreadystatechange = function() {
	if (this.readyState == 4 && this.status == 200) {
		var jsonData = JSON.parse(this.responseText);
		action(jsonData);
	}
}

//Returns the query string from the current URL
xmlhttp.open("GET", json, true);
xmlhttp.send();

function getQueryVariables() {
	var search = window.location.search.replace(/\/+$/, "");
	var query  = search.substring(1);
	var vars   = query.split('&');
	return vars;
}

function getQueryVariable(variable) { // Thanks http://stackoverflow.com/questions/2090551/parse-query-string-in-javascript#2091331
	var vars = getQueryVariables();
		
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split('=');

		if (decodeURIComponent(pair[0]) == variable) {
      			return decodeURIComponent(pair[1]);
    		}
	}
return null;
}